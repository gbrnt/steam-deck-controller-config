#!/usr/bin/env python

"""
Convert JSON file to Valve VDF
From https://gist.github.com/ynsta/7221512c583fbfbafe6d
(with a slight modification to remove extra newlines and change some spaces to tabs)
"""

import json

def json2vdf(stream):

    """
    Read a json file and return a string in Steam vdf format
    """

    def _istr(ident, string):
        return (ident * '\t') + string

    data = json.loads(stream.read(), object_pairs_hook=list)

    def _json2vdf(data, indent):
        out = ''
        for k, v in data:
            if isinstance(v, list):
                out += _istr(indent, '"{}"\n'.format(k))
                out += _istr(indent, '{\n')
                out += _json2vdf(v, indent + 1)
                out += _istr(indent, '}\n')
            else:
                out += _istr(indent,'"{}"\t\t"{}"\n'.format(k, v))
        return out

    return  _json2vdf(data, 0)


def main():
    """
    Read json and write Steam vdf conversion
    """
    import sys
    import argparse
    parser = argparse.ArgumentParser(prog='json2vdf', description=main.__doc__)
    parser.add_argument('-i', '--input',
                        default=sys.stdin,
                        type=argparse.FileType('r'),
                        help='input json file (stdin if not specified)')
    parser.add_argument('-o', '--output',
                        default=sys.stdout,
                        type=argparse.FileType('w'),
                        help='output vdf file (stdout if not specified)')

    args = parser.parse_args()
    args.output.write(json2vdf(args.input))

if __name__ == '__main__':
    main()
