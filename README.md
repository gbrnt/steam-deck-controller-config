# Steam Deck Controller config files

The Steam Deck controller config files are stored in the folder `/home/deck/.local/share/Steam/steamapps/common/Steam Controller Configs/<userid>/config`. Within that folder there are subfolders for each game, named with the ID of the game. Inside each game folder is a file called `controller_neptune.vdf`, which has the controller configuration in Valve's VDF format (Neptune was the Steam Deck's codename during development).

## Structure

The VDF format is similar to a JSON file, but keys and values are separated by whitespace, and key:value pairs are separated by newlines. Here's the rough structure in bullet point format (sorry, this is very nested! I've also removed some less important looking parts).

* `controller_mappings`: Top level object
    * `group`: A group of actions which can be bound to a control source - there are multiple of these
        * `id`: The number of the group
        * `mode`: The type of group that this is, for example `four_buttons`, `dpad`, `joystick_move`
        * `inputs`: The actual inputs that this is a group of. The contents here will depend on what `mode` of group it is
            * Example would be `button_a`: `activators`: `Full_Press`: `bindings`: `binding` `xinput_button A`
        * Active groups also get a `gameactions` attribute, which seems to always be empty (so far)
    * `preset`: Mapping groups to the controls ("sources") of the Deck - perhaps there can be multiple different presets?
        * `id`: "0"
        * `name`: `Default`
        * `group_source_bindings`: Key-value pairs where the keys are group numbers and the values are the control source to map them to as well as `active` or `inactive`
            * Example: `"13"     "right_trackpad active"` would mean that the right trackpad is mapped to group 13, and this mapping is active

For the full structure have a look at the `basic_gamepad` example.

## Examples

This repo contains examples of different controller configuration options, starting from a basic gamepad file with only minimal changes. Note that these were all made using the game CrossCode, with Steam game ID 368340.

To see what each example changes, I recommend using a diff tool. For example using `vimdiff`: `cd examples; vimdiff basic_gamepad/controller_neptune.vdf keyboard-bindings.controller_neptune.vdf`. There are also structural diff tools for the files which have been converted to JSON, but I haven't tried that yet.

Ignore any changes in the top section of the file, as they're just variations in how I made the mappings. There are also a *lot* more options available for each of these examples that I haven't yet tried customising.

### Basic gamepad

Just a basic gamepad, that's all! Nothing on the trackpads or the back grip buttons.

### Blank

This example is based on the basic gamepad with all bindings removed (or as many as I could in the UI anyway).

### Keyboard bindings

This example shows some of the options you can map to for keyboard keys, particularly combining multiple keypresses (e.g. Shift+A). I've put them in a radial menu just to get a lot of options grouped together.

### Trackpad

For all of these I have taken the basic gamepad example and set the right trackpad to do something.

#### As joystick

Use the trackpad as a joystick. Not clear if you can change which joystick it acts as.

#### As mouse

Use the trackpad as a mouse.

#### Button pad

Use the trackpad as a four-button pad, like the "ABXY" cluster on an Xbox controller. Not sure what order the buttons are in.

#### Directional swipe

Map trackpad swipes to the top, bottom left or right to button presses. You can also map a click on the trackpad.

#### Directional pad

Use the trackpad as a d-pad, either 8-way, 4-way or emulating an analog stick.

#### Radial menu

Brings up a radial menu - you can place your thumb in a section of the trackpad and click to select that option. Maps clockwise from the top (north) in equal segments. So if you add four actions, option 1 will be up, 2 right, 3 down and 4 left.

#### Single button

Does what it says on the tin. You can also make it do something on touch as well as on click.
